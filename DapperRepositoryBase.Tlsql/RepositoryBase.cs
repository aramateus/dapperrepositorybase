﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperRepositoryBase;

namespace DapperRepositoryBase.PlSql
{
    /// <summary>
    /// Repository base is based on Micro-ORM Dapper, it has all the artifacts necessary to do Read on ORACLE.
    /// </summary>
    /// <typeparam name="T">Entity POCO</typeparam>
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        /// <summary>
        /// Current DbConnection.
        /// </summary>
        protected IDbConnection DbConnection { get; private set; }
        /// <summary>
        /// Current commandTimeout.
        /// </summary>
        protected int CommandTimeout { get; set; }
        public int Count()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> Get()
        {
            return Get<T>();
        }
        /// <summary>
        /// Get a specific element over the condition "Where [Key] = [Value]".
        /// </summary>
        /// <param name="value">Condition Where [Key] = [Value].</param>
        /// <returns>The entity found.</returns>
        public T Get(KeyValuePair<string, string> value)
        {
            return Get<T>(value);
        }
        /// <summary>
        /// Get a specific element over the condition "Where [Key] = [Value]".
        /// </summary>
        /// <param name="value">Condition Where [Key] = [Value].</param>
        /// <returns>The entity found.</returns>
        public T Get(KeyValuePair<string, int> value)
        {
            return Get<T>(value);
        }

        public IEnumerable<T> GetList(KeyValuePair<string, string> value)
        {
            return GetList<T>(value);
        }

        public IEnumerable<T> GetList(KeyValuePair<string, int> value)
        {
            return GetList<T>(value);
        }

        /// <summary>
        /// Get all.
        /// </summary>
        /// <typeparam name="TX">Entity POCO</typeparam>
        /// <returns></returns>
        protected IEnumerable<TX> Get<TX>()
        {
            var tablename = GetTableName(typeof(TX));
            var query = string.Format("SELECT * FROM {0}", tablename);
            if (DbConnection.State == ConnectionState.Closed) DbConnection.Open();
            return DbConnection.Query<TX>(query, commandTimeout: CommandTimeout);

        }
        /// <summary>
        /// Get a specific entity over the condition "Where [Key] = [Value]".
        /// <typeparam name="TX"></typeparam>
        /// <param name="value">Condition Where [Key] = [Value].</param>
        /// <returns></returns>
        /// </summary>
        protected TX Get<TX>(KeyValuePair<string, string> value)
        {
            var tableName = GetTableName(typeof(TX));
            var query = string.Format("SELECT * FROM {0} WHERE {1} = '{2}'", tableName, value.Key, value.Value);
            if (DbConnection.State == ConnectionState.Closed) DbConnection.Open();
            var result = DbConnection.Query<TX>(query).FirstOrDefault();
            return result;


        }
        /// <summary>
        /// Get a specific element over the condition "Where [Key] = [Value]".
        /// </summary>
        /// <param name="value">Condition Where [Key] = [Value].</param>
        /// <returns>The entity found.</returns>
        public virtual TX Get<TX>(KeyValuePair<string, int> value)
        {
            var tableName = GetTableName(typeof(TX));
            var query = string.Format("SELECT * FROM {0} WHERE {1} = {2}", tableName, value.Key, value.Value);
            if (DbConnection.State == ConnectionState.Closed) DbConnection.Open();
            var result = DbConnection.Query<TX>(query, new { value.Key, value.Value }).FirstOrDefault();
            return result;
        }

        /// <summary>
        /// Get all elements over the condition "Where [Key] = [Value]".
        /// </summary>
        /// <param name="value">Condition Where [Key] = [Value].</param>
        /// <returns>List the entity found.</returns>
        protected IEnumerable<TX> GetList<TX>(KeyValuePair<string, int> value)
        {
            var tableName = GetTableName(typeof(TX));
            var query = string.Format("SELECT * FROM {0} WHERE {1} = {2}", tableName, value.Key, value.Value);
            if (DbConnection.State == ConnectionState.Closed) DbConnection.Open();
            var result = DbConnection.Query<TX>(query, new { value.Key, value.Value }).ToList();
            return result;
        }

        /// <summary>
        /// Get all elements over the condition "Where [Key] = [Value]".
        /// </summary>
        /// <param name="value">Condition Where [Key] = [Value].</param>
        /// <returns>List the entity found.</returns>
        protected IEnumerable<TX> GetList<TX>(KeyValuePair<string, string> value)
        {
            var tableName = GetTableName(typeof(TX));
            var query = string.Format("SELECT * FROM {0} WHERE {1} = '{2}'", tableName, value.Key, value.Value);
            if (DbConnection.State == ConnectionState.Closed) DbConnection.Open();
            var result = DbConnection.Query<TX>(query).ToList();
            return result;
        }

        /// <summary>
        /// Get a number of rows.
        /// </summary>
        /// <returns></returns>
        protected int Count<TX>()
        {

            var tableName = GetTableName(typeof(TX));
            var query = string.Format("SELECT Count(*) FROM {0} ", tableName);
            if (DbConnection.State == ConnectionState.Closed) DbConnection.Open();
            var result = DbConnection.ExecuteScalar<int>(query);
            return result;
        }

        /// <summary>
        /// Get the tablename form attribute [TableName] if is defiend, otherwise return name of entity poco. 
        /// </summary>
        /// <param name="type">Entity Poco.</param>
        /// <returns></returns>
        protected string GetTableName(Type type)
        {
            if (Attribute.IsDefined(type, typeof(TableName)))
            {
                var attr = Attribute.GetCustomAttribute(type, typeof(TableName));
                return ((TableName)attr).TableNameWithScheme;
            }
            return type.Name;
        }
    }
}
